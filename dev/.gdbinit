python

import sys
sys.path.insert(0, '/home/gm/kde/src/extragear/kdevelop/kdevelop/plugins/gdb/printers')
from qt import register_qt_printers
register_qt_printers(None)

# see https://github.com/Lekensteyn/qt5printers
# see prettyprint_ex1
#
#import sys, os.path
#sys.path.insert(0, os.path.expanduser('~/.gdb'))
#import qt5printers
#qt5printers.register_printers(gdb.current_objfile())

end

set print pretty 1
