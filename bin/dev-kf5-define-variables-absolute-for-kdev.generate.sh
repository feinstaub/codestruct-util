#!/bin/bash

TARGET=dev-kf5-define-variables-absolute-for-kdev

# get the actual kf5 environment
. dev-kf5-env-setup

# and then get the actual absolute variable values

echo "# AUTO-GENERATED with dev-kf5-define-variables-absolute-for-kdev.generate.sh" > $TARGET
echo "# NOTE: we removed the 'export' in order to do Copy&Paste for KDevelop's Environment Batch Edit Mode" >> $TARGET
echo "# For RUNTIME (not build time)" >> $TARGET
echo "" >> $TARGET
echo "# minimum (?):" >> $TARGET
env | grep -e ^XDG_DATA_DIRS= >> $TARGET
env | grep -e ^XDG_CONFIG_DIRS= >> $TARGET
env | grep -e ^PATH= >> $TARGET
env | grep -e ^QT_PLUGIN_PATH= >> $TARGET
env | grep -e ^QML2_IMPORT_PATH= >> $TARGET
env | grep -e ^QML_IMPORT_PATH= >> $TARGET
echo "" >> $TARGET
echo "# use development home dir (instead of from current user):" >> $TARGET
env | grep -e ^XDG_DATA_HOME= >> $TARGET
env | grep -e ^XDG_CONFIG_HOME= >> $TARGET
env | grep -e ^XDG_CACHE_HOME= >> $TARGET
echo "" >> $TARGET

echo ""
echo $TARGET written.
