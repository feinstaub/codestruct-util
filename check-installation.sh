#!/bin/bash

###################################################
errorReport_init () {
    # creates an empty file
    HAS_ERROR_FILE=$(mktemp)
}

errorReport_add () {
    echo "$1" >> $HAS_ERROR_FILE
    printf "\n\n" >> $HAS_ERROR_FILE
}

errorReport_print_summary () {
    if [ -s "$HAS_ERROR_FILE" ] ; then
        echo ❌
        echo "❌ We have one or more ERRORS:"
        echo ❌
        echo "------------------------------"
        cat $HAS_ERROR_FILE
        echo "------------------------------"
    else
        echo
        echo "✅ Everything seems to be fine"
        echo
    fi
}

errorReport_report_to_Desktop () {
    if [ -s "$HAS_ERROR_FILE" ] ; then
        DESKTOP_FILE=$HOME/Desktop/ERROR-ERROR-ERROR.txt

        printf "A script has problems:\n\n" >> $DESKTOP_FILE
        cat $HAS_ERROR_FILE >> $DESKTOP_FILE

        echo "Error reported to: $DESKTOP_FILE"
    fi
}

errorReport_close () {
    rm $HAS_ERROR_FILE
}
###################################################

HEAD_CMD="head -n 3"

# TODO: check if those are really symlinks

errorReport_init

echo
FILE=$HOME/.config/fish/conf.d/my-init.fish
echo "====== CHECK ====== $FILE"
$HEAD_CMD $FILE && echo "......OK......" || (echo ".......NOT OK......" ; ls -la $HOME/.config/fish/conf.d/ ; errorReport_add "Problem with $FILE")
[ -L $FILE ] || (echo "........NOT OK..... because not a symlink")
echo

echo
FILE=$HOME/.config/git/attributes
echo "====== CHECK ====== $FILE"
$HEAD_CMD $FILE && echo "......OK......" || (echo ".......NOT OK......" ; ls -la $HOME/.config/git/ ; errorReport_add "Problem with $FILE")
[ -L $FILE ] || (echo "........NOT OK..... because not a symlink")
echo

echo
FILE=$HOME/.gitconfig
echo "====== CHECK ====== $FILE"
$HEAD_CMD $FILE && echo "......OK......" || (echo ".......NOT OK......" ; ls -la $HOME ; errorReport_add "Problem with $FILE")
[ -L $FILE ] || (echo "........NOT OK..... because not a symlink")
echo

echo
FILE=$HOME/.gitignore
echo "====== CHECK ====== $FILE"
$HEAD_CMD $FILE && echo "......OK......" || (echo ".......NOT OK......" ; ls -la $HOME ; errorReport_add "Problem with $FILE")
[ -L $FILE ] || (echo "........NOT OK..... because not a symlink")
echo

echo
FILE=odt2txt
echo "====== CHECK ====== $FILE"
which $FILE && echo "......OK......" || (echo ".......NOT OK......" ; errorReport_add "Problem with $FILE")
echo

echo
FILE=markdown-toc
echo "====== CHECK ====== $FILE"
which $FILE && echo "......OK......" || (echo ".......NOT OK......" ; errorReport_add "Problem with $FILE")
echo

errorReport_print_summary
errorReport_report_to_Desktop
errorReport_close
