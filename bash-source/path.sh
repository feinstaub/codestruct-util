




# See https://userbase.kde.org/Session_Environment_Variables
# Symlink this file here: $HOME/.config/plasma-workspace/env/path.sh
#      NO, DO NOT DO THIS... NOTE - putting path.sh also there (aside from bash and fish config) would result in duplicate paths
#       Warum ist das so? Vielleicht, weil ich irgendwann mal (wie?) fish als Standardshell gesetzt habe.

# keep in sync with my-init.fish
export PATH=$PATH:~/bin
export PATH=$PATH:~/.local/bin
export PATH=$PATH:~/node_modules/.bin
export PATH=$PATH:~/go/bin
export PATH=$PATH:$CODESTRUCTROOT/bin
export PATH=$PATH:~/dataDocuments/rc/bin
