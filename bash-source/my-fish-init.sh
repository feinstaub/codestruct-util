#!/bin/bash

# Make fish the default login shell:
# $ chsh -s $(which fish)
# ohne sudo!
# Dann neu in Plasma einloggen

TARGETDIR=~/.config/fish/conf.d
TARGET=$TARGETDIR/my-init.fish
mkdir -p $TARGETDIR
rm -f $TARGET
ln -s $(pwd)/my-init.fish ~/.config/fish/conf.d/
echo "Symlink created to my-init.fish: $TARGET"
