# reload without relogin: source ~/.bashrc # see http://stackoverflow.com/questions/2518127/how-do-i-reload-bashrc-without-logging-out-and-back-in
# all about commands like \u etc.: http://wiki.ubuntuusers.de/Bash/Prompt

#$(git rev-parse --show-toplevel)
#~/dataDocuments/aux/codestruct-util
CODESTRUCTROOT=$(realpath "$(dirname $BASH_SOURCE)/..")


# personal bin; NOTE: keep in sync with fish
# echo "Set bin PATHs including jekyll"
#
source $HOME/dataDocuments/repo-sources/codestruct-util/bash-source/path.sh






#
# export PATH=$PATH:/usr/lib64/ruby/gems/2.1.0/gems/jekyll-3.1.0/bin

# Außer Betrieb seit Migration im Dezember 2021
# git completion mit TAB
# echo "Set git completion"
#source $CODESTRUCTROOT/bash-source/git-completion.src.sh
# set prompt
#source $CODESTRUCTROOT/bash-source/set-prompt.src.sh

# optionally override to another prompt:
# function __prompt_command() {
#     PS1=""
#     # DEFAULT: PS1+="\[$(ppwd)\]\u@\h:\w>"
#     PS1+="\[$(ppwd)\]user@tumbleweed:\w> "
#     test
# }

# more startup stuff see 'tool-plasma-my-autostart'

#echo "my-init-bash-source.sh complete"
