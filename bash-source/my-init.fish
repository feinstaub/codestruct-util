#
# INSTALL to make work: symlink my-init.fish to: **$HOME/.config/fish/conf.d/**
# see my-fish-init.sh
#                           NOTE that there might be more files,
#                                   like /home/gm/.config/fish/conf.d/kdesrc-build.fish
#                                   which adds: fish_add_path --global /home/gm/dev/kde/src/kdesrc-build

# http://fishshell.com/docs/current/index.html

# https://stackoverflow.com/questions/34196283/how-to-retrieve-the-current-scripts-path-in-fish-shell
set CODESTRUCTROOT (realpath (dirname (readlink -f (status --current-filename)))/..)

# NOTE: keep in sync with path.sh
#
set -x PATH $PATH ~/bin
set -x PATH $PATH ~/.local/bin
set -x PATH $PATH ~/node_modules/.bin
set -x PATH $PATH ~/go/bin
set -x PATH $PATH $CODESTRUCTROOT/bin
set -x PATH $PATH ~/dataDocuments/rc/bin
#
#set -x PATH $PATH /usr/lib64/ruby/gems/2.1.0/gems/jekyll-3.1.0/bin
