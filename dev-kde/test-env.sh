#!/bin/bash

TARGET=dev-kf5-define-variables-absolute-for-kdev-test

# get the actual kf5 environment
. dev-kf5-env-setup

# and then get the actual absolute variable values

echo "# AUTO-GENERATED with dev-kf5-define-variables-absolute-for-kdev.generate.sh" > $TARGET
echo "# NOTE: we removed the 'export' in order to do Copy&Paste for KDevelop's Environment Batch Edit Mode" >> $TARGET
echo "# For RUNTIME (not build time)" >> $TARGET
echo "" >> $TARGET
echo "# minimum (?):" >> $TARGET
env  >> $TARGET

echo "" >> $TARGET

echo ""
echo $TARGET written.
