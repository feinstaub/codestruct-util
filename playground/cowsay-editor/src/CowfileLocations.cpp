#include "CowfileLocations.h"

#include <QStandardPaths>
#include <QUrl>

std::vector<CowfileLocation> get_cowfile_locations()
{
    // Config file: ~/.config/cowsay-editor/cowsay-editor.conf
    // Custom cowfiles: ~/.config/cowsay-editor/custom-cowfiles/

    QUrl dir1 = QUrl::fromLocalFile(QStandardPaths::writableLocation(QStandardPaths::HomeLocation) +
                                                           "/.config/cowsay-editor/custom-cowfiles/"
                                                          );
    QUrl dir2 = QUrl::fromLocalFile(QStandardPaths::writableLocation(QStandardPaths::HomeLocation) + "/Documents");

    return { { dir1, true },
             { dir2, false }
    };
}
