#pragma once

#include "MainView.h"
#include <QMainWindow>

namespace Ui {
class CowsayEditor;
}

class CowsayEditor : public QMainWindow
{
    Q_OBJECT

public:
    explicit CowsayEditor(QWidget *parent = 0);
    ~CowsayEditor();

public slots:

private:

private:
    Ui::CowsayEditor *ui;
    MainView* mainView;
};

namespace {
    void init_responses();
    const CowData& random_response();
}
