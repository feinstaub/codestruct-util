#pragma once

#include "SayOrThink.h"
#include <QString>
#include <optional>

struct CowData
{
    QString text;
    SayOrThink sayorthink = SayOrThink::Say;

    /**
     * Overrides all the following attributes
     */
    QString raw_args;

    /**
     * e.g. built-in "turkey" or full path
     */
    QString cowfile; // -f

    std::optional<char> provided_mode;
    std::optional<QString> eye_string; // -e
    std::optional<QString> tongue_string; // -T
    int word_wrap = 40; // default is -W 40
    bool word_wrap_off = false; // -n
};
