#include "CowsayProcess.h"

#include "CowData.h"
#include "Speech.h"
#include <QProcess>
#include <QDebug>

CowsayProcess::CowsayProcess(QObject *parent) : QObject(parent)
{
}

std::vector<QString> CowsayProcess::GetBuiltinCowfilenames()
{
    QProcess p;
    p.start("cowsay -l");

    std::vector<QString> result;

    connect(&p, &QProcess::readyReadStandardOutput, this, [&p, &result]() {
        QByteArray out = p.readAllStandardOutput();
        auto a = out.split(':');
        Q_ASSERT(a.size() == 2);
        auto b = a[1].split('\n');
        for (const auto& line : b) {
            for (const auto& s : line.split(' ')) {
                if (s.size() > 0) {
                    result.push_back(s);
                }
            }
        }

    });

    p.waitForFinished();

    //Speech s;
    //s.test1();

    return result;
}

QString CowsayProcess::GetCommandline(const CowData& cowData)
{
    QString command;

    if (cowData.sayorthink == SayOrThink::Say) {
        command = "cowsay";
    } else {
        command = "cowthink";
    }

    if (!cowData.raw_args.isEmpty()) {
        command += " " + cowData.raw_args;
    } else {
        if (cowData.cowfile != "" && cowData.cowfile != "default") {
            command += QString(" -f %1").arg(cowData.cowfile);
        }
        if (cowData.provided_mode) {
            command += QString(" -%1 ").arg(QString(cowData.provided_mode.value()));
        }
        if (cowData.eye_string) {
            command += QString(" -e \"%1\"").arg(cowData.eye_string.value());
        }
        if (cowData.tongue_string) {
            command += QString(" -T \"%1\"").arg(cowData.tongue_string.value());
        }
        if (cowData.word_wrap != 40) {
            command += QString(" -W %1").arg(cowData.word_wrap);
        }
        if (cowData.word_wrap_off) {
            command += " -n"; // must be last!
        }
    }

    return command;
}

QString CowsayProcess::CallCowsay(const CowData& cowData)
{
    QProcess p;
    QString command = GetCommandline(cowData);

    p.start(command);

    QString result;

    connect(&p, &QProcess::readyReadStandardOutput, this, [&p, &result]() {
        QByteArray out = p.readAllStandardOutput();
        result = out;
    });

    p.write(cowData.text.toUtf8());
    p.closeWriteChannel();

    p.waitForFinished();

    return result;
}
