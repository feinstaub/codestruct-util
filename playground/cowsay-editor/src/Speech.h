#pragma once

#include <QObject>
#ifdef Qt5TextToSpeech_FOUND
#include <QTextToSpeech>

class Speech : public QObject
{
    Q_OBJECT
public:
    explicit Speech(QObject *parent = 0);
    ~Speech();

    void test1();

private:
    QTextToSpeech *m_speech = nullptr;
};

#endif
