#include "MainView.h"
#include "ui_MainView.h"
#include "Cowtput.h"
#include "EditorWidget.h"

#include <QDebug>
#include <QMainWindow>
#include <QPushButton>
#include <QSizePolicy>
#include <QSplitter>
#include <QStatusBar>

MainView::MainView(QWidget *parent, QMainWindow* mainWindow) :
    QWidget(parent),
    ui(new Ui::MainView)
{
    ui->setupUi(this);

    // View Tab
    cowtput_ = new Cowtput(this);
    confCow = new ConfigureCowWidget(this);
    connect(confCow, &ConfigureCowWidget::cowChanged, cowtput_, &Cowtput::displayCow);
    QSplitter *splitter = new QSplitter(Qt::Horizontal, ui->tabView);
    splitter->addWidget(confCow);
    splitter->addWidget(cowtput_);
    splitter->setHandleWidth(2); // TODO: separator not visible :(
    splitter->setSizes({ 150, 250 });
    ui->tabViewLayout->addWidget(splitter);
    confCow->emitCowChanged();
    confCow->setFocusToInputText();
    if (mainWindow) {
        connect(cowtput_, &Cowtput::reportCommandline, this,
                [mainWindow](const QString& cmd) {
                    mainWindow->statusBar()->showMessage(cmd);
                });
    } else {
        qWarning() << "mainWindow nullptr leads to no status bar message.";
    }

    // Edit Tab
    auto tabEditLayout = new QVBoxLayout(this);
    ui->tabEdit->setLayout(tabEditLayout);
    editorWidget = new EditorWidget(this);
    editorCowtput = new Cowtput(this);
    QSplitter *editSplitter = new QSplitter(Qt::Horizontal, ui->tabView);
    editSplitter->addWidget(editorWidget);
    editSplitter->addWidget(editorCowtput);
    editSplitter->setHandleWidth(2);
    tabEditLayout->addWidget(editSplitter);
    editSplitter->setSizes({ 250, 250 });

    // Main
    // ui->tabWidget->setCurrentIndex(0); // todo: uncomment to show View first
}

MainView::~MainView()
{
    delete ui;
}

Cowtput* MainView::cowtput()
{
    return cowtput_;
}
