#pragma once

#include <QWidget>

class QPaintEvent;

enum class CowCharType {
    Char = 0,
    Thoughts,
    Eyes,
    Tongue
};

struct CowChar
{
    CowCharType type;
    QString value; // one char
};

class CowFileEditArea : public QWidget
{
    Q_OBJECT

public:
    explicit CowFileEditArea(QWidget *parent = 0);
    ~CowFileEditArea();

protected:
    virtual void paintEvent(QPaintEvent *event) override;

signals:

public slots:

private:
    std::vector<CowChar> cowstring;
};

std::vector<CowChar> string_to_cowchar(const QString& str);
QPoint i_to_pos(const std::vector<CowChar>& cowstring, int index);
