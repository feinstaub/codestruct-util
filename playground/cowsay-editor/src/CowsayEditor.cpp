#include "CowsayEditor.h"
#include "ui_CowsayEditor.h"
#include "AboutDialog.h"
#include "Cowtput.h"

#include <cstdlib>
#include <ctime>

CowsayEditor::CowsayEditor(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CowsayEditor)
{
    init_responses();

    ui->setupUi(this);
    mainView = new MainView(this, this);
    Cowtput* cowtput = mainView->cowtput();
    setCentralWidget(mainView);
    statusBar()->showMessage("This tool was made possible with the powerful 'cowsay' engine", 3000);

    connect(ui->actionQuit, &QAction::triggered, this, [this]() { QApplication::quit(); });

    connect(ui->actionGiveMeMilk, &QAction::triggered, this, [cowtput]() {
        cowtput->displayCow(random_response());
    });

    connect(ui->actionAbout, &QAction::triggered, this, [this]() {
        AboutDialog aboutDialog(this);
        aboutDialog.exec();
    });
}

CowsayEditor::~CowsayEditor()
{
    delete ui;
}

namespace {

std::vector<CowData> responses;

void init_responses()
{
    std::srand(std::time(nullptr));
    CowData data;
    data.text = "How old are you?";
    responses.push_back(data);
    data.text = "Are you still breast-feeding?";
    responses.push_back(data);
    data.text = "Oh boy, I thought we were past this.";
    responses.push_back(data);
    data.text = "Oh boy, it is <current year>, we should be past this a long time ago.";
    responses.push_back(data);
}

const CowData& random_response()
{
    int r = std::rand();
    return responses[r % responses.size()];
}

}
