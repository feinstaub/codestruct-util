#pragma once

#include "ConfigureCowWidget.h"
#include <QWidget>

namespace Ui {
class MainView;
}

class QMainWindow;
class Cowtput;
class EditorWidget;

class MainView : public QWidget
{
    Q_OBJECT

public:
    /**
     * mainWindow used for status bar
     */
    explicit MainView(QWidget *parent = 0, QMainWindow* mainWindow = nullptr);
    ~MainView();

    Cowtput* cowtput();

private:
    Ui::MainView *ui;
    Cowtput* cowtput_;
    ConfigureCowWidget* confCow;

    EditorWidget* editorWidget;
    Cowtput* editorCowtput;
};
