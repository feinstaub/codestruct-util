#include <QtTest/QtTest>

#include "StoryMode.h"
#include <tuple>

QString testStoryNoBody = R"({ "story_title": "zen 0", "args": "-f small" })";

QString testStory1 = R"({ "story_title": "zen 1", "args": "-f small" }
Was machst du, um dich zu entspannen?
{ }
Nichts.)";

QString testStoryNewlines = R"({ "story_title": "zen 2" }

AAA

{ }

BBB
)";

QString testStoryMultiline = R"({ "story_title": "zen 3" }
Zeile 1
Zeile 2
Zeile 3
{ }

Hallo 4

)";

QString testStoryComment = R"({ "story_title": "zen 4", "args": "-f small" }
Was machst du, um dich zu entspannen?
{ } /* comment */
Nichts.)";



class StoryModeTest: public QObject
{
    Q_OBJECT

private slots:
    void myFirstTest()
    {
        QVERIFY(true); // check that a condition is satisfied
        QCOMPARE(1, 1); // compare two values
    }

    void parse_invalidstart()
    {
        std::optional<StoryMode::CowsayStory> story;
        QString error;
        std::tie(story, error) = StoryMode::parse_story("hallo");
        QVERIFY(error.contains("JSON"));
    }

    void parse_invalidjson()
    {
        std::optional<StoryMode::CowsayStory> story;
        QString error;
        std::tie(story, error) = StoryMode::parse_story("{ hallo");
        QVERIFY(error.contains("JSON"));
    }

    void parse_notitle()
    {
        std::optional<StoryMode::CowsayStory> story;
        QString error;
        std::tie(story, error) = StoryMode::parse_story("{ \"hallo\": \"test\" }");
        QVERIFY(error.contains("story_title"));
    }

    void parse_nobody()
    {
        std::optional<StoryMode::CowsayStory> story;
        QString error;
        std::tie(story, error) = StoryMode::parse_story(testStoryNoBody);
        QCOMPARE(story.value().title, "zen 0");
        QCOMPARE(story.value().pages.size(), 0);
    }

    void parse_title()
    {
        std::optional<StoryMode::CowsayStory> story;
        QString error;
        std::tie(story, error) = StoryMode::parse_story(testStory1);
        QCOMPARE(story.value().title, "zen 1");
    }

    void parse_pages()
    {
        std::optional<StoryMode::CowsayStory> story;
        QString error;
        std::tie(story, error) = StoryMode::parse_story(testStory1);
        QCOMPARE(story.value().title, "zen 1");
        QCOMPARE(story.value().pages.size(), 2);
        QCOMPARE(story.value().pages[0].text, "Was machst du, um dich zu entspannen?");
        QCOMPARE(story.value().pages[1].text, "Nichts.");
    }

    void parse_args()
    {
        std::optional<StoryMode::CowsayStory> story;
        QString error;
        std::tie(story, error) = StoryMode::parse_story(testStory1);
        QCOMPARE(story.value().title, "zen 1");
        QCOMPARE(story.value().pages.size(), 2);
        QCOMPARE(story.value().pages[0].args, "-f small");
        QCOMPARE(story.value().pages[1].args, "");
    }

    void parse_newlines()
    {
        std::optional<StoryMode::CowsayStory> story;
        QString error;
        std::tie(story, error) = StoryMode::parse_story(testStoryNewlines);
        QCOMPARE(story.value().pages.size(), 2);
        QCOMPARE(story.value().pages[0].text, R"(
AAA

)");
        QCOMPARE(story.value().pages[1].text, R"(
BBB)");
    }

    void parse_multilines()
    {
        std::optional<StoryMode::CowsayStory> story;
        QString error;
        std::tie(story, error) = StoryMode::parse_story(testStoryMultiline);
        QCOMPARE(story.value().pages.size(), 2);
        QCOMPARE(story.value().pages[0].text, R"(Zeile 1
Zeile 2
Zeile 3)");
        QCOMPARE(story.value().pages[1].text, R"(
Hallo 4

)");
    }

    void parse_comments_not_possible_right_now()
    {
        std::optional<StoryMode::CowsayStory> story;
        QString error;
        std::tie(story, error) = StoryMode::parse_story(testStoryComment);
        //QCOMPARE(story.value().pages.size(), 2);
        QCOMPARE(story.value().pages.size(), 1);
    }
};

QTEST_MAIN(StoryModeTest)
#include "StoryModeTest.moc"
