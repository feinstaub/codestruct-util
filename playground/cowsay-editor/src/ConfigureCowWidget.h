#pragma once

#include <QWidget>
#include "CowData.h"
#include "StoryMode.h"
#include <vector>
#include <tuple>

namespace Ui {
class ConfigureCowWidget;
}

class QTimer;

class ConfigureCowWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ConfigureCowWidget(QWidget *parent = 0);
    ~ConfigureCowWidget();

    void emitCowChanged();
    void setFocusToInputText();

signals:
    void cowChanged(const CowData& cowData);

private:
    void emitCowChangedByComboIndex(int index);
    CowData cowDataFromUi();
    void playNextPage();
    void onStoryModeSwitchedOn();
    void onStoryTextChanged(const QString& storyText);
    void onPlayerChanged();
    void onPlayerTimerTick();

private:
    Ui::ConfigureCowWidget *ui;
    std::vector<std::pair<QString, StoryMode::CowsayStory>> stories;
    std::optional<StoryMode::StoryPlayer> storyPlayer;
    QTimer* storyTimer;
};
