#pragma once

#include <QWidget>

namespace Ui {
class Cowtput;
}

class CowData;

class Cowtput : public QWidget
{
    Q_OBJECT

public:
    explicit Cowtput(QWidget *parent = 0);
    ~Cowtput();

signals:
    void reportCommandline(QString cmd);

public slots:
    void displayCow(const CowData& cowData);

private:

private:
    Ui::Cowtput *ui;
};
