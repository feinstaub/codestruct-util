#pragma once

#include <QWidget>

namespace Ui {
class EditorWidget;
}

class EditorWidget : public QWidget
{
    Q_OBJECT

public:
    explicit EditorWidget(QWidget *parent = 0);
    ~EditorWidget();

signals:

public slots:

private:

private:
    Ui::EditorWidget *ui;
};
