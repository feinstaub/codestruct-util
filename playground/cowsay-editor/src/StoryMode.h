#pragma once

#include "SayOrThink.h"
#include <QString>
#include <optional>
#include <tuple>
#include <vector>

namespace StoryMode {

// ----------------------------------------------

struct StoryPage
{
    QString text;
    QString args;
    SayOrThink sayorthink;
};

// ----------------------------------------------

class CowsayStory
{
public:
    QString title;
    std::vector<StoryPage> pages;
};

// ----------------------------------------------

/**
    * QString filled in case of error
    */
std::tuple<std::optional<CowsayStory>, QString> parse_story(const QString& s);
std::vector<std::pair<QString, CowsayStory>> get_builtin_stories();

// ----------------------------------------------

class StoryPlayer
{
public:
    /**
     * true on success; else false and use get_error() to see the error
     */
    bool init(const QString& text);
    QString get_error();

    int position();
    int num_pages();
    bool is_last_page();
    bool is_playing();

    /**
     * pointer lives as long StoryPlayer lives or until next init() call
     */
    StoryPage* start_playing();
    StoryPage* pause_playing();
    StoryPage* stop_playing();
    StoryPage* next_page();
    StoryPage* prev_page();
    /**
     * 0..num_pages()-1 allowed
     */
    StoryPage* seek(int position);
    StoryPage* current_page();

private:
    StoryPage messagePage;
    std::optional<CowsayStory> story;
    QString error;

    /**
     * -1: not started / stopped
     * 0: first page
     */
    int position_ = -1;

    bool playing = false;
};

// ----------------------------------------------

}
