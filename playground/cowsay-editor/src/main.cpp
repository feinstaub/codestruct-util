#include <QApplication>
#include <QDebug>
#include "CowsayEditor.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    CowsayEditor w;
    w.show();

    return app.exec();
}
