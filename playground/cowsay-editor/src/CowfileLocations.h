#pragma once

#include <QUrl>
#include <vector>

struct CowfileLocation
{
    QUrl url;
    bool editable = false;
};

std::vector<CowfileLocation> get_cowfile_locations();
