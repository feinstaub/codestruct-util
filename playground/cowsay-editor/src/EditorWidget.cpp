#include "EditorWidget.h"
#include "ui_EditorWidget.h"
#include "CowFileEditArea.h"

#include <QScrollBar>
#include <QPushButton>

EditorWidget::EditorWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EditorWidget)
{
    ui->setupUi(this);

    ui->editorFrameRemove->setVisible(false);
    ui->horizontalLayoutInsertHere->addWidget(new CowFileEditArea(this));
}

EditorWidget::~EditorWidget()
{
    delete ui;
}
