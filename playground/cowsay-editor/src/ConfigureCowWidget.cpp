#include "ConfigureCowWidget.h"
#include "ui_ConfigureCowWidget.h"

#include "CowsayProcess.h"
#include "CowfileLocations.h"
#include "StoryMode.h"
#include <QApplication>
#include <QtGlobal>
#include <QAction>
#include <QIcon>
#include <QMenu>
#include <QDebug>
#include <QTimer>

ConfigureCowWidget::ConfigureCowWidget(QWidget *parent) :
    QWidget(parent),
    storyTimer(new QTimer(this)),
    ui(new Ui::ConfigureCowWidget)
{
    ui->setupUi(this);
    ui->frameMoreOptions->setVisible(false);
    ui->frameRunOptions->setVisible(false);
    ui->groupBoxStoryTelling->setVisible(false);

    connect(ui->checkBoxMoreOptions, &QCheckBox::stateChanged, this,
            [this](int state) {
                ui->frameMoreOptions->setVisible(state == Qt::Checked);
                ui->frameRunOptions->setVisible(state == Qt::Checked);
            });

    // Fill cowfiles combo
    ui->comboBoxUsrCowfiles->addItem("default"); // todo: better replace with "" and remove this special case in CowsayProcess.cpp
    CowsayProcess cp;
    for (const QString& item : cp.GetBuiltinCowfilenames()) {
        if (item != "default") {
            ui->comboBoxUsrCowfiles->addItem(item);
        }
    }

    // Connect main 'cow file' Combo
    connect(ui->comboBoxUsrCowfiles, qOverload<int>(&QComboBox::highlighted),
            this, &ConfigureCowWidget::emitCowChangedByComboIndex);
    connect(ui->comboBoxUsrCowfiles, qOverload<int>(&QComboBox::currentIndexChanged), // will not be sent if nothing was changed
    //connect(ui->comboBoxUsrCowfiles, qOverload<int>(&QComboBox::activated),
            this, &ConfigureCowWidget::emitCowChangedByComboIndex);
    // clumsy hack, but not solved:
    connect(qApp, &QApplication::focusChanged, this,
            [this](QWidget *old, QWidget *now) {
                if (old == ui->comboBoxUsrCowfiles) {
                    emitCowChanged();
                }
            });

    // connect further change events
    connect(ui->plainTextEdit, &QPlainTextEdit::textChanged, this,
            [this]() {
                if (storyPlayer) {
                    onStoryTextChanged(ui->plainTextEdit->toPlainText());
                } else {
                    emitCowChanged();
                }
            });

    std::vector<std::pair<QString, char>> modes = {
        { "None", ' ' },
        { "Borg mode", 'b' },
        { "Appears dead", 'd' },
        { "Greedy mode", 'g' },
        { "State of paranoia", 'p' },
        { "Stoned", 's' },
        { "Tired", 't' },
        { "Wired mode", 'w' },
        { "Youthful appearance", 'y' }
    };
    for (auto& t : modes) {
        ui->comboBoxProvidedMode->addItem(t.first, t.second);
    }
    connect(ui->comboBoxProvidedMode, qOverload<int>(&QComboBox::currentIndexChanged), this,
            [this]() {
                emitCowChanged();
            });

    connect(ui->checkBoxCustomEyes, &QCheckBox::stateChanged, this,
            [this](int) {
                emitCowChanged();
            });
    connect(ui->lineEditEyes, &QLineEdit::textChanged, this,
            [this]() {
                emitCowChanged();
            });
    connect(ui->checkBoxCustomTongue, &QCheckBox::stateChanged, this,
            [this](int) {
                emitCowChanged();
            });
    connect(ui->lineEditTongue, &QLineEdit::textChanged, this,
            [this]() {
                emitCowChanged();
            });

    connect(ui->spinBoxWordWrap, qOverload<int>(&QSpinBox::valueChanged), this,
            [this](int) {
                emitCowChanged();
            });
    connect(ui->checkBoxWordWrapOff, &QCheckBox::stateChanged, this,
            [this](int) {
                emitCowChanged();
            });

    connect(ui->radioButtonSay, &QRadioButton::toggled, this,
            [this](bool) {
                emitCowChanged();
            });

    connect(ui->checkBoxStoryTellingMode, &QCheckBox::stateChanged, this,
            [this](int state) {
                bool story_on = state == Qt::Checked;
                ui->groupBoxStoryTelling->setVisible(story_on);
                if (story_on) {
                    onStoryModeSwitchedOn();
                } else {
                    storyPlayer = std::nullopt;
                    emitCowChanged();
                }
            });

    connect(ui->pushButtonStoryPlay, &QPushButton::clicked, this,
            [this]() {
                if (storyPlayer) {
                    if (storyPlayer->is_playing()) {
                        storyPlayer->pause_playing();
                    } else {
                        storyPlayer->start_playing();
                    }
                    onPlayerChanged();
                }
            });
    connect(ui->pushButtonStoryStop, &QPushButton::clicked, this,
            [this]() {
                if (storyPlayer) {
                    storyPlayer->stop_playing();
                    onPlayerChanged();
                }
            });
    connect(ui->pushButtonStorySeekBack, &QPushButton::clicked, this,
            [this]() {
                if (storyPlayer) {
                    storyPlayer->prev_page();
                    onPlayerChanged();
                }
            });
    connect(ui->pushButtonStorySeekForward, &QPushButton::clicked, this,
            [this]() {
                if (storyPlayer) {
                    storyPlayer->next_page();
                    onPlayerChanged();
                }
            });

    connect(ui->sliderStoryPlayPosition, &QSlider::valueChanged, this,
            [this](int value) {
                if (storyPlayer) {
                    if (storyPlayer->position() != value) {
                        qDebug() << "sliderStoryPlayPosition valueChanged / value " << value << " player position " << storyPlayer->position() << " seek to value";
                        storyPlayer->seek(value);
                        onPlayerChanged();
                    }
                }
            });

    stories = StoryMode::get_builtin_stories();
    int index = 0;
    for (const std::pair<QString, StoryMode::CowsayStory>& story : stories) {
        auto a = new QAction(story.second.title, this);
        a->setData(index);
        connect(a, &QAction::triggered, this,
                [this, a]() {
                    const QString& orig_string = stories[a->data().toInt()].first;
                    storyPlayer = StoryMode::StoryPlayer(); // reset storyPlayer
                    ui->plainTextEdit->setPlainText(orig_string);
                    qDebug() << "after setPlainText";
                    storyPlayer->stop_playing();
                });
        ui->toolButtonLoadStory->addAction(a);
        index++;
    }

    connect(storyTimer, &QTimer::timeout, this, &ConfigureCowWidget::onPlayerTimerTick);
    onPlayerChanged();

    // fill CowfileLocations
    for (const CowfileLocation& cfloc : get_cowfile_locations()) {
        qDebug() << cfloc.url;
        qDebug() << cfloc.url.toLocalFile();
        auto a = new QAction(cfloc.url.toLocalFile(), this);
        a->setData(cfloc.url);
        connect(a, &QAction::triggered, this,
                [this, a]() {
                    //const QString& orig_string = stories[a->data().toInt()].first;
                    //storyPlayer = StoryMode::StoryPlayer(); // make storyPlayer non-null
                    //ui->plainTextEdit->setPlainText(orig_string);
                });
        if (!cfloc.editable) {
            auto delAction = new QAction(QIcon::fromTheme("remove"), "Delete", this);
            // todo: connect
            QMenu* m = new QMenu(this);
            a->setMenu(m);
            a->menu()->addAction(delAction);
        }
        ui->toolButtonCowfileLocations->addAction(a);
    }
    auto addAction = new QAction(QIcon::fromTheme("list-add"), "Add...", this);
    // todo: connect
    ui->toolButtonCowfileLocations->addAction(addAction);

    // initially select all text
    ui->plainTextEdit->selectAll();
}

ConfigureCowWidget::~ConfigureCowWidget()
{
    delete ui;
}

void ConfigureCowWidget::emitCowChanged()
{
    // provided mode overrides custom eye and tongue settings
    bool provided_mode = ui->comboBoxProvidedMode->currentIndex() > 0;
    ui->lineEditEyes->setEnabled(!provided_mode);
    ui->checkBoxCustomEyes->setEnabled(!provided_mode);
    ui->lineEditTongue->setEnabled(!provided_mode);
    ui->checkBoxCustomTongue->setEnabled(!provided_mode);

    emitCowChangedByComboIndex(ui->comboBoxUsrCowfiles->currentIndex());
}

void ConfigureCowWidget::setFocusToInputText()
{
    ui->plainTextEdit->setFocus();
}

void ConfigureCowWidget::emitCowChangedByComboIndex(int index)
{
    CowData cowData = cowDataFromUi();
    cowData.cowfile = ui->comboBoxUsrCowfiles->itemText(index);
    emit cowChanged(std::move(cowData));
}

CowData ConfigureCowWidget::cowDataFromUi()
{
    CowData result;

    result.text = ui->plainTextEdit->toPlainText();

    result.cowfile = ui->comboBoxUsrCowfiles->currentText();

    char mode = ui->comboBoxProvidedMode->currentData().toChar().toLatin1();
    if (mode != ' ') {
        result.provided_mode = mode;
    }

    result.eye_string = ui->checkBoxCustomEyes->isChecked() ? static_cast<std::optional<QString>>(ui->lineEditEyes->text()) : std::nullopt;
    result.tongue_string = ui->checkBoxCustomTongue->isChecked() ? static_cast<std::optional<QString>>(ui->lineEditTongue->text()) : std::nullopt;

    result.sayorthink = ui->radioButtonSay->isChecked() ? SayOrThink::Say : SayOrThink::Think;

    result.word_wrap = ui->spinBoxWordWrap->value();
    result.word_wrap_off = ui->checkBoxWordWrapOff->isChecked();

    return result;
}

void ConfigureCowWidget::onStoryModeSwitchedOn()
{
    QString storyText = ui->plainTextEdit->toPlainText();
    storyPlayer = StoryMode::StoryPlayer(); // reset storyPlayer
    if (storyPlayer->init(storyText)) {
        onPlayerChanged();
    }
}

void ConfigureCowWidget::onStoryTextChanged(const QString& storyText)
{
    qDebug() << "textChanged storyPlayer valid";
    int lastPos = storyPlayer->position(); // remember last pos
    if (storyPlayer->init(storyText)) {
        storyPlayer->seek(lastPos); // restore if possible
    } else {
        storyPlayer = std::nullopt;
        ui->checkBoxStoryTellingMode->setChecked(false);
    }
    onPlayerChanged();
}

void ConfigureCowWidget::onPlayerChanged()
{
    bool playEnabled = storyPlayer.has_value();
    bool playIsPause = false;
    bool stopEnabled = storyPlayer.has_value();
    bool forwardEnabled = storyPlayer.has_value();
    bool backEnabled = storyPlayer.has_value();
    bool sliderEnabled = storyPlayer.has_value();
    int sliderPos = -1;
    int sliderMax = 0;

    if (storyPlayer) {
        playEnabled = true;
        StoryMode::StoryPage* page = storyPlayer->current_page();

        if (storyPlayer->is_playing()) {
            playIsPause = true;

            // calc duration
            int pageDurationUi = ui->spinBoxPageDuration->value();
            int durationMillisBase = pageDurationUi * 100;
            // assume that this time is needed for 40 characters
            // if we have more than 40 chars than we add more time accordingly
            int textsize = page->text.size();
            int durationMillis = durationMillisBase;
            if (textsize > 40) {
                durationMillis += ((textsize - 40) / 40.0) * durationMillisBase;
            }
            qDebug() << "interval " << durationMillis;
            storyTimer->setInterval(durationMillis);

            // start time if not already running
            if (!storyTimer->isActive()) {
                storyTimer->start();
            }
        } else {
            playIsPause = false;
        }

        qDebug() << "onPlayerChanged / position " << storyPlayer->position() << " num_pages " << storyPlayer->num_pages();
        sliderPos = storyPlayer->position();
        sliderMax = storyPlayer->num_pages() - 1;

        CowData cowData;
        cowData.text = page->text;
        cowData.raw_args = page->args;
        emit cowChanged(std::move(cowData));
    }

    ui->pushButtonStoryPlay->setEnabled(playEnabled);
    if (playIsPause) {
        ui->pushButtonStoryPlay->setIcon(QIcon::fromTheme("media-playback-pause"));
    } else {
        ui->pushButtonStoryPlay->setIcon(QIcon::fromTheme("media-playback-start"));
    }
    ui->pushButtonStoryStop->setEnabled(stopEnabled);
    ui->pushButtonStorySeekForward->setEnabled(forwardEnabled);
    ui->pushButtonStorySeekBack->setEnabled(backEnabled);
    ui->sliderStoryPlayPosition->setEnabled(sliderEnabled);
    ui->sliderStoryPlayPosition->setMaximum(sliderMax); // triggers valuechanged if silderMax was decreased
    ui->sliderStoryPlayPosition->setValue(sliderPos); // must support -1
}

void ConfigureCowWidget::onPlayerTimerTick()
{
    if (storyPlayer && storyPlayer->is_playing()) {
        storyPlayer->next_page();
        if (storyPlayer->is_last_page()) {
            storyPlayer->pause_playing();
        }
        onPlayerChanged();
    } else {
        storyTimer->stop();
    }
}
