#pragma once

enum class SayOrThink
{
    Say,
    Think
};
