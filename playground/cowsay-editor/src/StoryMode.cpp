#include "StoryMode.h"

#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QTextStream>

namespace StoryMode {

std::vector<QString> get_builtin_story_strings()
{
    std::vector<QString> result;

    // https://de.spiritualwiki.org/Wiki/ZenGeschichten
    result.push_back(R"({ "story_title": "Zen - Entspannung", "args": "-f small" }
Was machst du, um dich zu entspannen?
{ }
Nichts.
{ "args": "-n" }
Wenn ich gehe, gehe ich,
wenn ich esse, esse ich,
und wenn ich schlafe, schlafe ich.
{ "args": "-f small -e \"??\"" }
Aber das tun doch alle.
{ "args": "-w" }
Eben nicht!)");

    // https://de.spiritualwiki.org/Wiki/ZenGeschichten
    result.push_back(R"({ "story_title": "Zen - Entspannung, bunny version", "args": "-f small" }
Was machst du, um dich zu entspannen?
{ "args": "-f bunny" }
...fragt der Schüler seinen Meister
{ }
Nichts
{ "args": "-f bunny" }
...erwiderte der Meister
{ "args": "-n" }
Wenn ich gehe, gehe ich,
wenn ich esse, esse ich,
und wenn ich schlafe, schlafe ich.
{ "args": "-f small" }
Das tun doch alle,
{ "args": "-f bunny" }
...meinte der Schüler darauf
{ }
Eben nicht!
{ "args": "-f bunny" }
...antwortete der Meister.
)");

    // https://de.wikipedia.org/wiki/Zen
    result.push_back(R"a({ "story_title": "Was ist Zen? (1)" }
Was ist Zen?
{ }
Ich würde gerne irgend etwas anbieten, um Dir zu helfen, aber im Zen haben wir überhaupt nichts.
)a");

    // https://de.wikipedia.org/wiki/Zen
    result.push_back(R"a({ "story_title": "Was ist Zen? (2)" }
Was ist Zen?
{ }
Zen ist nicht etwas Aufregendes, sondern Konzentration auf deine alltäglichen Verrichtungen.
)a");

    result.push_back(R"({ "story_title": "2001: A Cow Odysee" }
Space.
{ }
A monolith.
{ }
All is full of stars...
)");

    result.push_back(R"({ "story_title": "Cow Trek" }
Der Weltraum.
{ }
Unendliche Weiten.
{ }
Wir befinden uns in einer fernen Zukunft.
)");

    result.push_back(R"({ "story_title": "2019: A Cow Odysee" }
Earth 2019.
{ }
All around the industrialized world.
{ }
Most of us cows are still bred by man and held captive.
{ }
The female half is forcefully impregnated over and over again.
{ }
Each has to give her calf's milk to man.
{ }
Until the body has worn out.
{ }
Each individual of the male half mostly may only live a short life.
{ }
In each case, it is man who ends these innocent lifes.
{ "args": "-f small" }
Help, please.
)");

    result.push_back(R"({ "story_title": "2019: A Cow Odysee, part 2" }
Sure, we cows have got less moral value than man.
{ }
Probably, because we are not capable of making those rules.
{ }
The real tragedy is three-fold.
{ }
1. Deep in his heart, man would really care more about us.
{ }
We see it how he treats other mammals like dogs or cats which are on a similar moral level as us.
{ }
Those are not designated as food and cared for in a kind manner.
{ }
2. Man was raised to think that we cows provide essential nutrition.
{ }
On the other hand, we see a respectable number of individuals who seem to thrive without our body fluids and body parts.
{ }
Nowadays, even many of man's own food scientists confirm that our services are not needed anymore in a modern society.
{ }
3. Psychology says that changing behaviour -- regardless how rational the reasoning -- is hard.
{ }
Especially, when there is fear that the new behaviour might separate this individual from its peer group.
{ }
FEAR NOT! 2020 will be the year of freeing yourself from your cheese addiction!
{ }
Cow tip: Use your communications networks to inform yourself of how we are treated in areas the advertisments will never show.
{ }
You might want to set a focus on breeding, slaughtering, your own emotions, logic and the plethora of healthy and yummy cow product-free food.
)");

    return result;
}

struct JsonLine
{
    QString story_title;
    QString args;
};

std::tuple<std::optional<CowsayStory>, QString> parse_story(const QString& s)
{
    std::tuple<std::optional<CowsayStory>, QString> result;
    CowsayStory story;
    QString error;

    auto add_storypage = [&story](StoryPage& storyPage) {
        if (storyPage.text.size() > 1
            && storyPage.text[storyPage.text.size() - 2] != "\n"
            && storyPage.text[storyPage.text.size() - 1] == "\n") {
            storyPage.text.chop(1);
        }
        story.pages.push_back(storyPage);
    };

    // for each line
    // if starts with { then treat as JSON

    int lineNumber = 0;
    QString input = s;
    QTextStream stream(&input);
    QString line; // current line
    StoryPage storyPage; // current StoryPage
    while (stream.readLineInto(&line)) {
        lineNumber++;

        JsonLine jsonline;
        bool is_jsonline = false;
        if (line.startsWith("{")) {
            QJsonDocument jsondoc = QJsonDocument::fromJson(line.toLatin1());
            if (!jsondoc.isNull()) {
                is_jsonline = true;
                QJsonObject obj = jsondoc.object();
                QJsonValue story_title = obj.value("story_title");
                QJsonValue args = obj.value("args");

                if (story_title.isString()) {
                    jsonline.story_title = story_title.toString();
                }
                if (args.isString()) {
                    jsonline.args = args.toString();
                }
            }
        }

        if (lineNumber == 1 && !is_jsonline) {
            error = "Line 1 does not contain a JSON line";
            break;
        }

        if (is_jsonline) {
            if (lineNumber == 1) {
                if (jsonline.story_title.isEmpty()) {
                    error = "Line 1 must contain a story_title";
                    break;
                }
                story.title = jsonline.story_title;
            }

            // add page if we reach a new json line and story page is not empty
            if (!storyPage.text.isEmpty()) {
                add_storypage(storyPage);
            }

            // reset page
            storyPage.args = jsonline.args;
            // todo: sayorthink
            storyPage.text = "";
        } else {
            if (line.isEmpty()) {
                storyPage.text += "\n";
            } else {
                storyPage.text += line;
                storyPage.text += "\n";
            }
        }
    }

    // add page if we reach the end
    if (!storyPage.text.isEmpty()) {
        add_storypage(storyPage);
    }

    if (error.isEmpty()) {
        return std::make_tuple(story, QString());
    } else {
        return std::make_tuple(std::nullopt, error);
    }
}

std::vector<std::pair<QString, CowsayStory>> get_builtin_stories()
{
    std::vector<std::pair<QString, CowsayStory>> result;

    for (const QString& s : get_builtin_story_strings()) {
        std::optional<CowsayStory> story;
        QString error;
        std::tie(story, error) = parse_story(s);
        if (story) {
            result.emplace_back(std::make_pair(s, story.value()));
        } else {
            qWarning() << "Parse Error: " << error;
        }
    }

    return result;
}

bool StoryPlayer::init(const QString& text)
{
    std::tie(story, error) = parse_story(text);
    position_ = -1;
    playing = false;
    return error.isEmpty();
}

QString StoryPlayer::get_error()
{
    return error;
}

int StoryPlayer::position()
{
    return position_;
}

int StoryPlayer::num_pages()
{
    return story->pages.size();
}

bool StoryPlayer::is_last_page()
{
    return position() == num_pages() - 1;
}

bool StoryPlayer::is_playing()
{
    return playing;
}

StoryPage* StoryPlayer::start_playing()
{
    playing = true;
    if (position() == -1) {
        return next_page();
    }

    return current_page();
}

StoryPage* StoryPlayer::pause_playing()
{
    playing = false;
    return current_page();
}

StoryPage* StoryPlayer::stop_playing()
{
    playing = false;
    position_ = -1;
    return current_page();
}

StoryPage* StoryPlayer::next_page()
{
    if (position() < num_pages() - 1) {
        position_++;
    } else {
        messagePage.text = "Invalid position";
        return &messagePage;
    }

    return current_page();
}

StoryPage* StoryPlayer::prev_page()
{
    if (position() > 0) {
        position_--;
    }

    return current_page();
}

StoryPage* StoryPlayer::seek(int pos)
{
    if (pos < 0 || pos >= num_pages()) {
        messagePage.text = "Invalid position";
        return &messagePage;
    } else {
        position_ = pos;
        return current_page();
    }
}

StoryPage* StoryPlayer::current_page()
{
    if (!error.isEmpty()) {
        messagePage.text = error;
        return &messagePage;
    } else if (position() == - 1) {
        messagePage.text = QString("Story '%1' loaded. Press Play to start.").arg(story->title);
        return &messagePage;
    } else {
        return &(story->pages[position_]);
    }
}

}
