#include "CowFileEditArea.h"

#include <QScrollBar>
#include <QPushButton>
#include <QPainter>
#include <QSizePolicy>

CowFileEditArea::CowFileEditArea(QWidget *parent) :
    QWidget(parent)
{
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    cowstring = string_to_cowchar("Hallo\nDas ist\nein Test.");
}

CowFileEditArea::~CowFileEditArea()
{

}

void CowFileEditArea::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

//     for (int x = 0; x < width(); x += 50) {
//         for (int y = 0; y < height(); y += 50) {
//             painter.drawLine(0, 0, x, y);
//         }
//     }

    for (int i = 0; i < cowstring.size(); i++)
    {
        QPoint pos = i_to_pos(cowstring, i);
        painter.drawText(pos, cowstring[i].value);
    }
}

std::vector<CowChar> string_to_cowchar(const QString& str)
{
    std::vector<CowChar> cowstring;

    for (const QChar& c : str) {
        cowstring.push_back(CowChar { CowCharType::Char, c });
    }

    return cowstring;
}

QPoint i_to_pos(const std::vector<CowChar>& cowstring, int index)
{
    int mx = 10;
    int my = 20;
    int x = mx;
    int y = my;
    int dx = 10;
    int dy = 15;
    for (int i = 0; i < cowstring.size(); i++)
    {
        if (cowstring[i].value != "\n") {
            x += dx;
        } else {
            x = mx;
            y += dy;
        }

        if (i == index) {
            break;
        }
    }

    return QPoint(x, y);
}
