#include "Cowtput.h"
#include "ui_Cowtput.h"
#include "CowData.h"
#include "CowsayProcess.h"

#include <QScrollBar>

Cowtput::Cowtput(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Cowtput)
{
    ui->setupUi(this);

    connect(ui->sliderFontSize, &QSlider::valueChanged, this,
            [this](int value) {
                QFont font = ui->plainTextEdit->font();
                font.setPointSize(value);
                ui->plainTextEdit->setFont(font);
            });
}

Cowtput::~Cowtput()
{
    delete ui;
}

void Cowtput::displayCow(const CowData& cowData)
{
    // save scroll position
    QScrollBar* sb1 = ui->plainTextEdit->horizontalScrollBar(); // apparently this is never null
    QScrollBar* sb2 = ui->plainTextEdit->verticalScrollBar(); // apparently this is never null
    int value1 = sb1->value();
    int value2 = sb2->value();

    CowsayProcess cp;
    ui->plainTextEdit->setPlainText(cp.CallCowsay(cowData));

    // restore
    sb1->setValue(value1);
    sb2->setValue(value2);

    emit reportCommandline(cp.GetCommandline(cowData));
}
