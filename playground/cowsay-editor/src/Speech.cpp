#include "Speech.h"

#ifdef Qt5TextToSpeech_FOUND

#include <QTextToSpeech>
#include <QDebug>
#include <QVoice>

Speech::Speech(QObject *parent) : QObject(parent)
{
    qDebug() << QTextToSpeech::availableEngines();
    m_speech = new QTextToSpeech("speechd", this);
    m_speech->setLocale(QLocale::system());
    auto vlist = m_speech->availableVoices();
    for (auto& v : vlist) {
        qDebug() << v.name();
    }
}

Speech::~Speech()
{
    delete m_speech;
}

void Speech::test1()
{
    m_speech->say("Hello");
}

#endif
