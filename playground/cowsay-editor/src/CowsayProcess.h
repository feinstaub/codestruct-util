#pragma once

#include <QObject>
#include <QString>
#include <vector>

class CowData;

class CowsayProcess : public QObject
{
    Q_OBJECT
public:
    explicit CowsayProcess(QObject *parent = 0);
    ~CowsayProcess() = default;

    /**
     * cowsay -l
     * e.g. { "bud-frogs", "bunny", "cheese" }
     */
    std::vector<QString> GetBuiltinCowfilenames();

    QString GetCommandline(const CowData& cowData);
    QString CallCowsay(const CowData& cowData);
};
