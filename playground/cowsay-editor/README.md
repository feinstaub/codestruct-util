CowsayEditor
============

Licence
-------
GPLv3

todo
----
- Jugendschutz-Mode (default: on, blendet einige built-in-Cowfiles aus)
- Cowfile editor
- Story telling mode
    - Keep away malicious args
    - Story categorization
- Cowfile locations
- Story telling mode with 3D View

Learn Open GL
-------------
* https://learnopengl.com/Introduction

Devel
-----
* Requirements
    * cmake (ninja)
    * libQt5Widgets-devel
    * libQt5Test-devel
    * optional
        * Qt5TextToSpeech
* Install libqt5-creator to edit .ui files and for Qt doc

Stories
-------
Sample Story-Format siehe unten.

https://de.spiritualwiki.org/Wiki/ZenGeschichten

    Vor der Erleuchtung: Holz hacken und Wasser holen,
    nach der Erleuchtung: Holz hacken und Wasser holen!


    Zwei Mönche waren unterwegs auf der Wanderschaft. Eines Tages gelangen sie ans Ufer eines Flusses, dessen Ufer durch eine Regenperiode aufgeweicht waren.

    Dort stand eine junge Frau in schönen, teuren Kleidern. Offenbar war sie im Begriff, den Fluss zu überqueren. Da das Wasser sehr tief war, hätte sie ihn nicht durchwaten können, ohne dabei ihre Kleider zu schädigen.

    Ohne zu zögern ging der ältere Mönch auf die Frau zu, hob sie auf seine Schultern und watete mit ihr durch das Wasser. Auf dem gegenüber liegenden Flussufer setzte er sie trockenen Fusses ab.

    Nachdem der jüngere Mönch ebenfalls den Fluss überquert hatte, setzten die beiden ihre Wanderung fort.

    Eine Stunde später fing Jüngere an, den seinen älteren Kameraden zu kritisieren: Bist du dir im Klaren, dass du nicht korrekt gehandelt hast, denn wie du weißt, ist es untersagt, näheren Kontakt mit Frauen zu haben oder mit ihnen zu sprechen. Und du hast sie sogar berührt. Wieso hast du gegen diese Regel verstoßen?

    Der Mönch, der die Frau über den Fluss getragen hatte, hörte sich die Vorwürfe des anderen mit Bedacht an. Dann antwortete er ruhig: Ich habe die Frau vor einer Stunde am Fluss abgesetzt. Wie erklärst du dir, dass du sie noch immer mit dir herumträgst?



    Ein junger Mann suchte einen Zen-Meister auf, um ihn zu fragen:
    Meister, wie lange wird es dauern, bis ich Befreiung erlangen werde?
    Vielleicht zehn Jahre,

    entgegnete der Meister.
    Und wie lange dauert es, wenn ich mich besonders anstrenge?%

    fragte der Schüler
    In diesem Fall kann es zwanzig Jahre dauern,

    erwiderte der Meister.
    Ich will so schnell wie möglich ans Ziel gelangen und bin bereit, wirklich jede Härte auf mich zu nehmen,

    beteuerte der Mann.
    Dann kann es bis zu vierzig Jahren dauern,

    erwiderte der Meister.


    Was machst du, um dich zu entspannen?
    [..., siehe unten]


Sample Story Format
-------------------
### Sample 1
{ "story_title": "Zen 1 try 1", "args": "-f small" } // "story_title" indicates a new story
Was machst du, um dich zu entspannen?                // "args" are cowsay arguments
{ "args": "-f bunny" }
...fragt der Schüler seinen Meister
{ } // default cowsay args
Nichts
{ "args": "-f bunny" }
...erwiderte der Meister
{ }
Wenn ich gehe, gehe ich,
wenn ich esse, esse ich,
und wenn ich schlafe, schlafe ich.
{ "args": "-f small" }
Das tun doch alle,
{ "args": "-f bunny" }
...meinte der Schüler darauf
{ }
Eben nicht!
{ "args": "-f bunny" }
...antwortete der Meister.
// nothing more in file indicates end of story

{ "cow": "say" }      // default / TODO
{ "cow": "think" }
