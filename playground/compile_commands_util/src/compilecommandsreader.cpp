/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2018  <copyright holder> <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "compilecommandsreader.h"
#include "compilecommandsitem.h"
#include <QFile>
#include <QTextStream>
#include <iostream>
#include <memory>
#include <fstream>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

struct CompileCommandsReader::Impl
{
    QString jsontext;
    json jsondata;
};

CompileCommandsReader::CompileCommandsReader()
    : impl { new CompileCommandsReader::Impl() } // TODO: why does make_unique not work?
{

}

CompileCommandsReader::~CompileCommandsReader() = default;

bool CompileCommandsReader::read_json(const QString& compile_commands_file)
{
    const bool useQt = false;

    json j;
    if (useQt) {
        QFile file(compile_commands_file);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            return false;
        }

        QTextStream in(&file);
        impl->jsontext = in.readAll();
        j = json::parse(impl->jsontext.toLatin1().data());
    } else {
        std::ifstream i(compile_commands_file.toLatin1().data());
        i >> j;
    }

    //std::cout << j.dump(4) << std::endl;

    return true;
}

std::vector<std::shared_ptr<const CompileCommandsItem>> CompileCommandsReader::find_for_file(const QString& filepath)
{
    std::vector<std::shared_ptr<const CompileCommandsItem>> result;
    //result.push_back(std::make_shared<CompileCommandsItem>());

    // TODO: read from json data and fill items etc.

    return result;
}
