#ifndef COMPILE_COMMANDS_UTIL_H
#define COMPILE_COMMANDS_UTIL_H

#include <QMainWindow>

namespace Ui {
class compile_commands_util;
}

class compile_commands_util : public QMainWindow
{
    Q_OBJECT

public:
    explicit compile_commands_util(QWidget *parent = 0);
    ~compile_commands_util();

private:
    Ui::compile_commands_util *ui;
};

#endif // COMPILE_COMMANDS_UTIL_H
