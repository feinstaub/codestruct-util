#include "compile_commands_util.h"
#include <QApplication>
#include <QDebug>
#include <iostream>
#include "compilecommandsreader.h"
#include "compilecommandsitem.h"

int main(int argc, char *argv[])
{
    CompileCommandsReader reader;
    if (!reader.read_json("../../sampledata/compile_commands.json")) {
        std::cout << "error reading file" << std::endl;
        exit(1);
    }
    const auto& result_list = reader.find_for_file("/home/gm/dev/markdown-in-kate/ktexteditorpreviewplugin/src/kpartview.cpp");
    if (result_list.size() == 0) {
        std::cout << "nothing found" << std::endl;
    }
    for (const auto& item : result_list) {
        qDebug() << item->debug_string();
    }

    std::cout << "List with all available translation units in source tree: ..." << std::endl;
    std::cout << "List with all available translation units in build dir: ..." << std::endl;

    exit(0);

    QApplication app(argc, argv);
    compile_commands_util w;
    w.show();

    return app.exec();
}

