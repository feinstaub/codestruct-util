Public codestruct-util
======================

Contains scripts and quick patches.

Public repo.

Check installation
------------------
use: $ check-installation.sh


View online
-----------
https://cgit.kde.org/scratch/gregormi/codestruct-util.git/


Admin
-----
Repo: see https://community.kde.org/Sysadmin/GitKdeOrgManual#Personal_scratch_repositories

CREATE
$ git remote add origin git@git.kde.org:scratch/gregormi/codestruct-util
$ git push --all origin

DELETE
$ ssh git@git.kde.org D trash scratch/gregormi/codestruct-patches

PUSH notes:
- real name audit

See also
--------
* https://github.com/balupton/dotfiles
