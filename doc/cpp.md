C++
===

Sprache
-------
* https://isocpp.org/tour
    * TODO!

* Templates
    * https://www.heise.de/developer/artikel/C-Core-Guidelines-Regeln-fuer-Template-Metaprogrammierung-4265182.html

* Projections
    * https://cukic.co/2019/01/22/projections-without-ranges/

* Vector and Movable
    * https://stackoverflow.com/questions/52330010/what-does-stdvector-look-like-in-memory
    * https://pagefault.blog/2018/03/01/common-misconception-with-cpp-move-semantics/

Debugging
---------
* see cpp-debugging.md

### print linker search path
* https://stackoverflow.com/questions/9922949/how-to-print-the-ldlinker-search-path

Compiler
--------
* https://gcc.gnu.org/projects/cxx-status.html - "C++ Standards Support in GCC"
* TODO
    * https://gcc.gnu.org/onlinedocs/gcc/Invoking-GCC.html
        * try out
    * https://gcc.gnu.org/onlinedocs/gcc/Preprocessor-Options.html#Preprocessor-Options
        * to find out which header files got included etc.

C++ Standard Library
--------------------
* Overview: https://en.cppreference.com/w/cpp/header
* std::optional: https://en.cppreference.com/w/cpp/utility/optional


Programming
-----------
### Reentrant function
* https://www.geeksforgeeks.org/reentrant-function/
* https://en.wikipedia.org/wiki/Reentrancy_(computing)

Building
--------
* ["GCC and Make - Compiling, Linking and Building - C/C++ Applications"](https://www3.ntu.edu.sg/home/ehchua/programming/cpp/gcc_make.html), 2018
    * todo
    * inkl. gängiger Tricks

LLVM / Clang
------------
* News
    * https://www.heise.de/developer/meldung/Compilerinfrastruktur-LLVM-7-0-mit-neuen-Werkzeugen-4166668.html

Blogs
-----
* https://pagefault.blog/about/
    * https://pagefault.blog/2019/01/16/dont-over-engineer/
    * https://pagefault.blog/2018/04/08/why-junior-devs-should-review-seniors-commits/
