Inbox 2019
==========

KDE
---
* https://jbbgameich.github.io/misc/2019/05/25/kdesrc-build-custom-qt.html - "Using kdesrc-build with a custom Qt"

Performance Analysis C++
------------------------
* http://llunak.blogspot.com/2019/05/linux-perf-and-kcachegrind.html - Linux perf and KCachegrind

git
---
* https://cullmann.io/posts/removing-files-from-git-history/
    * Removing Files from Git History
    * https://rtyley.github.io/bfg-repo-cleaner/

systemd
-------
* How to List All Running Services Under Systemd in Linux
    * https://www.tecmint.com/list-all-running-services-under-systemd-in-linux/
* https://wiki.archlinux.org/index.php/Systemd/User
    * todo

cgroups
-------
* https://wiki.archlinux.org/index.php/cgroups, todo
