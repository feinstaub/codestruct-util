Inbox 2018
==========

KDE and Docker
--------------
* KDevelop: www.proli.net/2017/05/23/kdevelop-runtimes-docker-and-flatpak-integration/
    * see user comments
        * https://github.com/Wenzel/docker-kdesrc-build
            * https://github.com/JanitorTechnology/dockerfiles

KDevelop
--------
* https://www.kdevelop.org/news/kdevelop-5280-released
    * Analyzers, Clazy
    * https://www.kdevelop.org/sites/www.kdevelop.org/files/inline-images/KDevelop%3A%20Clazy%20run%20on%20Okular.png
        * todo: how to use it? --> "Compile commands file 'dev/from-template/MyKdeApp1/build/compile_commands.json' does not exist."
        * https://github.com/KDE/clazy/blob/master/README.md
* Heaptrack
* "From code to related bug report or review in just a hover & click": https://frinring.wordpress.com/2018/09/09/from-code-to-related-bug-report-or-review-in-just-a-hover-click/
* clang-tidy
    * $ kdesrc-build kdev-clang-tidy
    * https://github.com/KDE/kdev-clang-tidy
    * todo: how to use it?

* Bugs
    * https://bugs.kde.org/show_bug.cgi?id=396661 - Crash in libclang

KDE Dev
-------
### Code Checking
* https://community.kde.org/Guidelines_and_HOWTOs/Code_Checking
* English Breakfast Network - http://ebn.kde.org/krazy/

Git
---
* Linux-Clients: https://www.maketecheasier.com/6-useful-graphical-git-client-for-linux/

### Git-Cola
* the work horse

### QGit
* is alive: https://github.com/tibirna/qgit/commits/master
* Filter by tree:
    * Select some paths in the tree and then see the history of those tree paths only
* --not master ?

### Guitar
* https://github.com/soramimi/Guitar

SW Dev Philosophy
-----------------
* Komplexität: https://www.heise.de/developer/artikel/Beten-wir-Komplexitaet-an-4170914.html
    * ...
    * User comment:
        * "Die Dinge sind meist nicht "komplex", weil man sie "komplex macht". Sie sind komplex, weil die zugrundeliegende Fragestellung und Anforderung komplex ist. Das gilt sowohl für die fachliche als auch die technische Ebene."
        * "Vor relativ kurzer Zeit wurden "traditionelle GUI Systeme" als zu komplex abgewatscht. Inzwischen haben "moderne" GUI Systeme wie der React/Redux Stack oder Ansible eine genauso hohe Komplexität erreicht. Nur ohne Typsicherheit (zu komplex, nicht agil genug) und mit schlechterer Ergonomie und Performance."
        * "Tatsächlich führt gerade die - falsche - Grundüberzeugung, dass die Komplexität nicht systemimmanent ist zu immer höherer Komplexität, beim hoffnungslosen Versuch etwas zu vereinfachen was nunmal nicht einfach ist, indem man ihm zunächst die Komplexität abspricht und diese danach wieder ranbaut. Viel sinnvoller wäre es zu lernen, wie man mit Komplexität richtig umgeht."

New to KDE
----------
* https://andrewcrouthamel.wordpress.com/2018/09/19/how-i-got-involved-in-kde/

Blog Systems
------------
### Hugo
* https://csoriano.pages.gitlab.gnome.org/csoriano-blog/post/2018-09-11-moving-from-wordpress

Linux
-----
### GNU Stow
* https://www.gnu.org/software/stow/
    * http://brandon.invergo.net/news/2012-05-26-using-gnu-stow-to-manage-your-dotfiles.html

### strace
* piping strace to grep: https://unix.stackexchange.com/questions/48223/piping-strace-to-grep
    * stderr

* see also:
    * bin/tangetools-tracefile
    * bin/dev-kapidox-kmoretools

### Firewall
* https://en.wikipedia.org/wiki/Uncomplicated_Firewall
