C++ Debugging
=============

Find memory leaks
-----------------
### Valgrind Memcheck
http://valgrind.org/docs/manual/quick-start.html

    valgrind --leak-check=yes myprog arg1 arg2

Warum sieht man keine Zeilen- und Sourceinfos, obwohl welche drin sind?

    ==6196== 2,018 (192 direct, 1,826 indirect) bytes in 4 blocks are definitely lost in loss record 7,067 of 7,126
    ==6196==    at 0x4C2E04F: malloc (in /usr/lib64/valgrind/vgpreload_memcheck-amd64-linux.so)
    ==6196==    by 0x720EF97: ??? (in /usr/lib64/libstdc++.so.6.0.25)
    ==6196==    by 0x1ED10EA1: ??? (in /home/gregor/kde/usr/lib64/plugins/ktexteditor/tabswitcherplugin.so)
    ==6196==    by 0x1ED1038A: ??? (in /home/gregor/kde/usr/lib64/plugins/ktexteditor/tabswitcherplugin.so)
    ==6196==    by 0x1ED100DC: ??? (in /home/gregor/kde/usr/lib64/plugins/ktexteditor/tabswitcherplugin.so)
    ==6196==    by 0x452322: ??? (in /home/gregor/kde/usr/bin/kate)
    ==6196==    by 0x451FA9: ??? (in /home/gregor/kde/usr/bin/kate)

    file /home/gregor/kde/usr/lib64/plugins/ktexteditor/tabswitcherplugin.so
    /home/gregor/kde/usr/lib64/plugins/ktexteditor/tabswitcherplugin.so: ELF 64-bit LSB pie executable x86-64, version 1 (GNU/Linux), dynamically linked, BuildID[sha1]=1797b5bed64b6cd245c08c927af421438b646d0c, with debug_info, not stripped

    file kate
    kate: ELF 64-bit LSB executable, x86-64, version 1 (GNU/Linux), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=0b925b6b1beed50b776eac581b7cb018e99063ae, with debug_info, not stripped

Generate core files with gdb
----------------------------
https://sourceware.org/gdb/onlinedocs/gdb/Core-File-Generation.html

    (gdb) generate-core-file

Analyse core files
------------------
todo: analyse core files

* https://stackoverflow.com/questions/5115613/core-dump-file-analysis

    gdb path/to/the/binary path/to/the/core

    (gdb) bt

### Which signal to send to create a core dump?
Is this ok?
    SIGABRT       6       Core    Abort signal from abort(3)

    $ kill -6 PID

    Aborted (core dumped)

### Where is the core dump?
    cat /proc/sys/kernel/core_pattern
    |/usr/lib/systemd/systemd-coredump %P %u %g %s %t %c %e

    man systemd-coredump
        in /var/lib/systemd/coredump

Not found.
Read more:
    https://wiki.archlinux.org/index.php/Core_dump


    gdb program core

### How to obtain application core dumps
https://www.suse.com/de-de/support/kb/doc/?id=3054866

    ulimit -c unlimited # To change limits permanently, install the ulimit package, configure the limits in the/etc/sysconfig/ulimit configuration file and reboot the system for the changed limits to take effect.

    install -m 1777 -d /var/local/dumps

    echo "/var/local/dumps/core.%e.%p" > /proc/sys/kernel/core_pattern
    # To additionally make this configuration change persistent over reboots, add a line kernel.core_pattern=/var/local/dumps/core.%e.%p to the /etc/sysctl.conf configuration file.

### Manually triggering a core dump
To send SIGABRT to process 1234, run

    kill -ABRT 1234

or to send SIGABRT to all running firefox-bin processes, run

    kill -ABRT $(pidof firefox-bin)

gdb pretty printers
-------------------
http://nikosams.blogspot.com/2009/10/gdb-qt-pretty-printers.html

see codestruct-util/dev/.gdbinit

Sample test program works.

A Kate core dump which was compiled with kdesrc-build does not work. With KDevelop Analyse Core File however it works.

"KF5 Static Builds": ELF Dissector
----------------------------------
* https://www.volkerkrause.eu/2018/10/13/kf5-static-builds.html
    * https://phabricator.kde.org/source/elf-dissector/
    * $ git clone git://anongit.kde.org/elf-dissector

Or better

    $ kdesrc-build elf-dissector

    ~/kde/build/playground/sdk/elf-dissector/bin> ./elf-dissector

Example:

    Open: ~/kde/build/kde/applications/kate/kate/kate

"Identifying all affected code is not always straightforward. Broad unit test coverage provides great value there, but ultimately you probably want to look at all method calls in the .init_array section of the dynamic library (or the corresponding non-ELF counter-part on other platforms), e.g. using a tool like ELF Dissector. Not everything in there is automatically a problem, but all problems will be in there."
