C++ Profiling
=============

Perf references
---------------
* Step-by-step for beginngers: https://dev.to/etcwilde/perf---perfect-profiling-of-cc-on-linux-of, 2017

    $ time ./fib
    $ perf record ./fib
    perf.data
    $ perf report
    "To get the call graph, we pass the -g option to perf record."
    $ perf record -g ./fib
    $ perf report # ...

* Example calls: http://www.brendangregg.com/perf.html, 2018

* Tutorial: https://perf.wiki.kernel.org/index.php/Tutorial, 2015

Example profilingex1 with Linux Perf
------------------------------------
* Create a new project from Template CMake / C++
* profilingex1
* Compile (debug symbols and and ninja are enabled by default)
    * see file profilingex1
    * although CMAKE_BUILD_TYPE is not set

    ninja -v
    [1/2] /usr/bin/c++     -MD -MT CMakeFiles/profilingex1.dir/main.cpp.o -MF CMakeFiles/profilingex1.dir/main.cpp.o.d -o CMakeFiles/profilingex1.dir/main.cpp.o -c ../main.cpp
    [2/2] : && /usr/bin/c++    -rdynamic CMakeFiles/profilingex1.dir/main.cpp.o  -o profilingex1   &&

-MD: dynamic
-MT: static
-MF:
       -MF file
           When used with -M or -MM, specifies a file to write the dependencies to.  If no -MF switch is given the preprocessor sends the rules to the same
           place it would send preprocessed output.

           When used with the driver options -MD or -MMD, -MF overrides the default dependency output file.

           If file is -, then the dependencies are written to stdout.

-c option says not to run the linker

-rdynamic
    Pass the flag -export-dynamic to the ELF linker, on targets that support it. This instructs the linker to add all symbols, not only used ones, to
    the dynamic symbol table. This option is needed for some uses of "dlopen" or to allow obtaining backtraces from within a program.

    $ perf record ./profilingex1
    [ perf record: Woken up 3 times to write data ]
    [kernel.kallsyms] with build id e8644576cec463f133c188474ea76301dce33449 not found, continuing without symbols
    [ perf record: Captured and wrote 0,790 MB perf.data (20668 samples) ]

    $ perf report
        # Annotate to get assembly

    $ perf record -g ./profilingex1

    $ perf report
        # note the new columns:     Children      Self
        # Enter to get into

perf report works fine.

TODO: hotspot still shows no symbols.

One hint see https://github.com/KDAB/hotspot
"To get backtraces, you will need to enable the dwarf callgraph mode:"

    $ perf record --call-graph dwarf ./profilingex1

    Warning:
    Processed 21508 events and lost 2 chunks!
    Check IO/CPU overload!
    [ perf record: Captured and wrote 162,339 MB perf.data (20167 samples) ]

    $ hotspot        # takes long time because of much console output
    $ nohup hotspot  # ok

hotspot still shows no symbols.

    TODO:
        - see known issues on https://github.com/KDAB/hotspot
        - https://unix.stackexchange.com/questions/276179/missing-stack-symbols-with-perf-events-perf-report-despite-fno-omit-frame-poi

Example Kate with Linux Perf
----------------------------
Build Kate with kdesrc-build

    $ sudo zypper install perf hotspot

    $ cd ~/kde/build/kde/applications/kate/
    $ . prefix.sh

    $ perf record -g kate
    $ hotspot perf.data

See no symbols in hotspot.

Rebuild with CMAKE_BUILD_TYPE=Debug

    $ perf record -g kate
    $ hotspot perf.data

    [ perf record: Woken up 15 times to write data ]
    [kernel.kallsyms] with build id e8644576cec463f133c188474ea76301dce33449 not found, continuing without symbols
    [ perf record: Captured and wrote 4,155 MB perf.data (96399 samples) ]

Still no symbols in hotspot.

    "Failed to find ELF file for an instruction pointer address. This can break stack unwinding and lead to missing symbols." TODO: how to solve?

https://unix.stackexchange.com/questions/276179/missing-stack-symbols-with-perf-events-perf-report-despite-fno-omit-frame-poi

Check compilation arguments:

    $ make kate VERBOSE=1

    cd /kde/build/kde/applications/kate/kate && ccache /usr/bin/c++  -DKActivities_FOUND -DKCOREADDONS_LIB -DKGUIADDONS_LIB -DQT_CONCURRENT_LIB -DQT_CORE_LIB -DQT_DBUS_LIB -DQT_GUI_LIB -DQT_NETWORK_LIB -DQT_NO_CAST_FROM_ASCII -DQT_NO_CAST_FROM_BYTEARRAY -DQT_NO_CAST_TO_ASCII -DQT_NO_SIGNALS_SLOTS_KEYWORDS -DQT_NO_URL_CAST_FROM_STRING -DQT_STRICT_ITERATORS -DQT_USE_QSTRINGBUILDER -DQT_WIDGETS_LIB -DQT_XML_LIB -D_GNU_SOURCE -D_LARGEFILE64_SOURCE -I/kde/build/kde/applications/kate/kate -I/kde/src/kde/applications/kate/kate -I/kde/build/kde/applications/kate/kate/kdeinit_kate_autogen/include -I/kde/build/kde/applications/kate -I/kde/src/kde/applications/kate/kate/session -I/kde/src/kde/applications/kate/kate/qtsingleapplication -isystem /kde/usr/include/KF5/KTextEditor -isystem /kde/usr/include/KF5 -isystem /kde/usr/include/KF5/KParts -isystem /kde/usr/include/KF5/KIOWidgets -isystem /kde/usr/include/KF5/KIOCore -isystem /kde/usr/include/KF5/KCoreAddons -isystem /usr/include/qt5 -isystem /usr/include/qt5/QtCore -isystem /usr/lib64/qt5/mkspecs/linux-g++ -isystem /kde/usr/include/KF5/KService -isystem /kde/usr/include/KF5/KConfigCore -isystem /usr/include/qt5/QtNetwork -isystem /usr/include/qt5/QtConcurrent -isystem /usr/include/qt5/QtDBus -isystem /kde/usr/include/KF5/KJobWidgets -isystem /usr/include/qt5/QtWidgets -isystem /usr/include/qt5/QtGui -isystem /kde/usr/include/KF5/KCompletion -isystem /kde/usr/include/KF5/KWidgetsAddons -isystem /kde/usr/include/KF5/KXmlGui -isystem /usr/include/qt5/QtXml -isystem /kde/usr/include/KF5/KConfigWidgets -isystem /kde/usr/include/KF5/KCodecs -isystem /kde/usr/include/KF5/KConfigGui -isystem /kde/usr/include/KF5/KAuth -isystem /kde/usr/include/KF5/KTextWidgets -isystem /kde/usr/include/KF5/SonnetUi -isystem /kde/usr/include/KF5/KI18n -isystem /kde/usr/include/KF5/KIconThemes -isystem /kde/usr/include/KF5/KWindowSystem -isystem /kde/usr/include/KF5/KGuiAddons -isystem /kde/usr/include/KF5/KDBusAddons -isystem /kde/usr/include/KF5/KCrash -isystem /kde/usr/include/KF5/KActivities  -pipe -std=c++0x -fno-operator-names -fno-exceptions -Wall -Wextra -Wcast-align -Wchar-subscripts -Wformat-security -Wno-long-long -Wpointer-arith -Wundef -Wnon-virtual-dtor -Woverloaded-virtual -Werror=return-type -Wvla -Wdate-time -pedantic -Wsuggest-override -Wlogical-op -Wzero-as-null-pointer-constant -g -fvisibility=hidden -fvisibility-inlines-hidden   -fPIC -std=gnu++11 -o CMakeFiles/kdeinit_kate.dir/katequickopen.cpp.o -c /kde/src/kde/applications/kate/kate/katequickopen.cpp
    [ 20%] Linking CXX static library libkdeinit_kate.a


### Compiled with debug symbols?

https://stackoverflow.com/questions/3284112/how-to-check-if-program-was-compiled-with-debug-symbols

    ~/kde/build/kde/applications/kate/kate> file kate
    kate: ELF 64-bit LSB executable, x86-64, version 1 (GNU/Linux), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=0b925b6b1beed50b776eac581b7cb018e99063ae, with debug_info, not stripped
    ~/kde/build/kde/applications/kate/kate> file ~/kde/usr/lib64/libKF5WindowSystem.so.5.51.0
    ~/kde/usr/lib64/libKF5WindowSystem.so.5.51.0: ELF 64-bit LSB pie executable x86-64, version 1 (GNU/Linux), dynamically linked, BuildID[sha1]=d8229a67d8cf67324f0ec9b64c5684179bd4fc18, with debug_info, not stripped

Look for "with debug_info".

todo: "Stripped or not has little to do with if it is built in debug or release. You can have a debug build that is stripped or a release build that is not stripped."

Example with no debug info:

    $ file /usr/bin/kate
    /usr/bin/kate: ELF 64-bit LSB pie executable x86-64, version 1 (GNU/Linux), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=5eceb2ed995914de081bfd661bbd2da298df93de, stripped

Use objdump:

    $ objdump --syms /usr/bin/kate

    /usr/bin/kate:     file format elf64-x86-64

    SYMBOL TABLE:
    no symbols

    $ objdump --syms kate | c++filt
    [...]
    0000000000000000       F *UND*  0000000000000000              KActivities::ResourceInstance::ResourceInstance(unsigned long long, QObject*)
    0000000000000000       F *UND*  0000000000000000              QString::QString(int, Qt::Initialization)@@Qt_5
    0000000000000000       O *UND*  0000000000000000              QAbstractButton::staticMetaObject@@Qt_5

