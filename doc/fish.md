Fish
====

GPLv2 (and others)

https://github.com/fish-shell/fish-shell

    sudo zypper install fish

Make default for login shells:

    chsh -s /usr/bin/fish

For Konsole go to the options and set it there.

https://github.com/oh-my-fish/oh-my-fish (inspired by oh-my-zsh)

    curl -L https://get.oh-my.fish | fish

    ...
    Installing package agnoster
    ✔ agnoster successfully installed.
    Installing package bobthefish
    ✔ bobthefish successfully installed.
    Installing package default
    ✔ default successfully installed.
    Installation successful!

Includes fancy git stuff: https://github.com/oh-my-fish/theme-bobthefish

More
----
* Tutorial: https://fishshell.com/docs/current/tutorial.html
* todo
    * https://www.ostechnix.com/oh-fish-make-shell-beautiful/
    * https://jvns.ca/blog/2017/04/23/the-fish-shell-is-awesome/

