KDE-Ideen
=========

KDing
-----
* 2019
    * Rescue KDing
        * https://phabricator.kde.org/T10719
            * WARTE
* 2018
    * https://en.opensuse.org/KDing
* 2017
    * "KDing?" - Rescue KDing. Mail an Maintainer. Keine Antwort.

Toolbox for Storage Devices
---------------------------
todo: report wish

1) Benchmark with Gnome Disks
2) check filesystem with GNOME Disks
3) Format with KDE Partition Manager
4) write ISO with GNOME Disks or SuseStudioImage Writer
5) Attempt Data Rescue with GParted

Recent files menu
-----------------
* Use prefix logic from Kate
* Use pin icon (QWidgetAction)
    * todo: report wish

Kate
----
### Misc
* WAIT
    * Kate: Bug 399807 - Enable the 'Document switcher' plugin by default
    * Kate: Bug 399810 - URL recognition + underline on hover + clickable
* Misc
    * https://bugs.kde.org/show_bug.cgi?id=344267 - Save files
        * currently closed due to inactivity
    * https://bugs.kde.org/show_bug.cgi?id=342594 - Document project plugin
        * currently closed due to inactivity

### Markdown
* Improve Syntax Highlighting colors
    * see https://phabricator.kde.org/D21293
        * todo: Regarding the screenshot, I see that "code 1, code2" is also orange. I think it would be better to change that as well, e.g. to blue as in "block quote" but a bit darker.
* https://bugs.kde.org/show_bug.cgi?id=386252
* https://bugs.kde.org/show_bug.cgi?id=390205 - KMarkdownWebView
* https://bugs.kde.org/show_bug.cgi?id=385897 - Click on relative URL tries to open this file
    * currently closed due to inactivity
* https://bugs.kde.org/show_bug.cgi?id=385546 - Sync cursor position / Scroll to view
    * currently closed due to inactivity
* https://bugs.kde.org/show_bug.cgi?id=386251 - Ability to view images in a document hierarchy like linode/docs
    * currently closed due to inactivity

### Python mode
* https://bugs.kde.org/show_bug.cgi?id=375362
    * currently closed due to inactivity

### CMake compile
* todo: impl or report wish
* cmake show compile commands
    * siehe JSON reader here playground/compile_commands_util/src/compilecommandsreader.h
* Display
    * with Formatting (not everything in one line but a new line for each option)
    * Praktische Kurzhilfe für Compiler-Optionen!
        * siehe auch GNU-GCC-Doku-Seiten

### Ability to reset Zoom level
* https://bugs.kde.org/show_bug.cgi?id=368296
    * currently closed due to inactivity

### DONE
* https://bugs.kde.org/show_bug.cgi?id=389484 - DONE
* https://bugs.kde.org/show_bug.cgi?id=389416 - DONE
* https://bugs.kde.org/show_bug.cgi?id=389415 - DONE

Plasma
------
### Welcome Applet for new users
todo: report wish

* Learn some useful shortcuts
    * Alt+Space for KRunner
    * Ctrl+Esc for System Monitor
* Set additional shortcuts for Windows users

### KWin-Toolbox
todo: report wish

* Zoom
* Wobbly Windows
