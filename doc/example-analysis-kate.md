Kate Debugging
==============

    ~/kde/build/kde/applications/kate/kate> valgrind --leak-check=yes  /home/gregor/kde/usr/bin/kate

    TODO: check qt usage
    QAbstractItemModel::endInsertRows:  Invalid index ( 3 , 1 ) in model detail::TabswitcherFilesModel(0xfd91a40)
    QAbstractItemModel::endInsertRows:  Invalid index ( 3 , 0 ) in model detail::TabswitcherFilesModel(0xfd91a40)
    QAbstractItemModel::endInsertRows:  Invalid index ( 3 , 1 ) in model detail::TabswitcherFilesModel(0xfd91a40)
    QAbstractItemModel::endInsertRows:  Invalid index ( 3 , 0 ) in model detail::TabswitcherFilesModel(0xfd91a40)


    TODO: why not debug infos from kate and libKF5TextEditor and libKF5Parts
    ==3267== 272 (8 direct, 264 indirect) bytes in 1 blocks are definitely lost in loss record 6,656 of 6,790
    ==3267==    at 0x4C2E04F: malloc (in /usr/lib64/valgrind/vgpreload_memcheck-amd64-linux.so)
    ==3267==    by 0x720EF97: ??? (in /usr/lib64/libstdc++.so.6.0.25)
    ==3267==    by 0x4050CF2: ??? (in /home/gregor/kde/usr/lib64/libKF5Parts.so.5.51.0)
    ==3267==    by 0x4050CA5: ??? (in /home/gregor/kde/usr/lib64/libKF5Parts.so.5.51.0)
    ==3267==    by 0x4F7FB5C: ??? (in /home/gregor/kde/usr/lib64/libKF5TextEditor.so.5.51.0)
    ==3267==    by 0x50B36D9: ??? (in /home/gregor/kde/usr/lib64/libKF5TextEditor.so.5.51.0)
    ==3267==    by 0x43D747: ??? (in /home/gregor/kde/usr/bin/kate)
    ==3267==    by 0x43DEE5: ??? (in /home/gregor/kde/usr/bin/kate)
    ==3267==    by 0x4573A4: ??? (in /home/gregor/kde/usr/bin/kate)
    ==3267==    by 0x45779B: ??? (in /home/gregor/kde/usr/bin/kate)==3267== 272 (8 direct, 264 indirect) bytes in 1 blocks are definitely lost in loss record 6,656 of 6,790
    ==3267==    at 0x4C2E04F: malloc (in /usr/lib64/valgrind/vgpreload_memcheck-amd64-linux.so)
    ==3267==    by 0x720EF97: ??? (in /usr/lib64/libstdc++.so.6.0.25)
    ==3267==    by 0x4050CF2: ??? (in /home/gregor/kde/usr/lib64/libKF5Parts.so.5.51.0)
    ==3267==    by 0x4050CA5: ??? (in /home/gregor/kde/usr/lib64/libKF5Parts.so.5.51.0)
    ==3267==    by 0x4F7FB5C: ??? (in /home/gregor/kde/usr/lib64/libKF5TextEditor.so.5.51.0)
    ==3267==    by 0x50B36D9: ??? (in /home/gregor/kde/usr/lib64/libKF5TextEditor.so.5.51.0)
    ==3267==    by 0x43D747: ??? (in /home/gregor/kde/usr/bin/kate)
    ==3267==    by 0x43DEE5: ??? (in /home/gregor/kde/usr/bin/kate)
    ==3267==    by 0x4573A4: ??? (in /home/gregor/kde/usr/bin/kate)
    ==3267==    by 0x45779B: ??? (in /home/gregor/kde/usr/bin/kate)
    ==3267==    by 0x44A74E: ??? (in /home/gregor/kde/usr/bin/kate)
    ==3267==    by 0x44A3AE: ??? (in /home/gregor/kde/usr/bin/kate)

    ==3267==    by 0x44A74E: ??? (in /home/gregor/kde/usr/bin/kate)
    ==3267==    by 0x44A3AE: ??? (in /home/gregor/kde/usr/bin/kate)

    ~/kde/build/kde/applications/kate/kate> valgrind --leak-check=full /home/gregor/kde/usr/bin/kate


    make VERBOSE=1

    [  2%] Building CXX object src/CMakeFiles/KF5TextEditor.dir/buffer/katetexthistory.cpp.o
    cd /home/gregor/kde/build/frameworks/ktexteditor/src && ccache /usr/bin/c++  -DKCOREADDONS_LIB -DKF5TextEditor_EXPORTS -DKGUIADDONS_LIB -DQT_CONCURRENT_LIB -DQT_CORE_LIB -DQT_DBUS_LIB -DQT_GUI_LIB -DQT_NETWORK_LIB -DQT_NO_CAST_FROM_ASCII -DQT_NO_CAST_FROM_BYTEARRAY -DQT_NO_CAST_TO_ASCII -DQT_NO_NARROWING_CONVERSIONS_IN_CONNECT -DQT_NO_SIGNALS_SLOTS_KEYWORDS -DQT_NO_URL_CAST_FROM_STRING -DQT_PRINTSUPPORT_LIB -DQT_QML_LIB -DQT_STRICT_ITERATORS -DQT_USE_QSTRINGBUILDER -DQT_WIDGETS_LIB -DQT_XML_LIB -DTRANSLATION_DOMAIN=\"ktexteditor5\" -D_GNU_SOURCE -D_LARGEFILE64_SOURCE -I/home/gregor/kde/build/frameworks/ktexteditor/src -I/home/gregor/kde/src/frameworks/ktexteditor/src -I/home/gregor/kde/build/frameworks/ktexteditor/src/KF5TextEditor_autogen/include -I/home/gregor/kde/build/frameworks/ktexteditor -I/home/gregor/kde/build/frameworks/ktexteditor/src/include -I/home/gregor/kde/src/frameworks/ktexteditor/src/include -I/home/gregor/kde/src/frameworks/ktexteditor/src/include/ktexteditor -I/home/gregor/kde/src/frameworks/ktexteditor/src/buffer -I/home/gregor/kde/src/frameworks/ktexteditor/src/completion -I/home/gregor/kde/src/frameworks/ktexteditor/src/dialogs -I/home/gregor/kde/src/frameworks/ktexteditor/src/document -I/home/gregor/kde/src/frameworks/ktexteditor/src/script -I/home/gregor/kde/src/frameworks/ktexteditor/src/mode -I/home/gregor/kde/src/frameworks/ktexteditor/src/render -I/home/gregor/kde/src/frameworks/ktexteditor/src/search -I/home/gregor/kde/src/frameworks/ktexteditor/src/syntax -I/home/gregor/kde/src/frameworks/ktexteditor/src/schema -I/home/gregor/kde/src/frameworks/ktexteditor/src/undo -I/home/gregor/kde/src/frameworks/ktexteditor/src/utils -I/home/gregor/kde/src/frameworks/ktexteditor/src/inputmode -I/home/gregor/kde/src/frameworks/ktexteditor/src/view -I/home/gregor/kde/src/frameworks/ktexteditor/src/swapfile -I/home/gregor/kde/src/frameworks/ktexteditor/src/variableeditor -isystem /home/gregor/kde/usr/include/KF5/KParts -isystem /home/gregor/kde/usr/include/KF5 -isystem /home/gregor/kde/usr/include/KF5/KIOWidgets -isystem /home/gregor/kde/usr/include/KF5/KIOCore -isystem /home/gregor/kde/usr/include/KF5/KCoreAddons -isystem /usr/include/qt5 -isystem /usr/include/qt5/QtCore -isystem /usr/lib64/qt5/mkspecs/linux-g++ -isystem /home/gregor/kde/usr/include/KF5/KService -isystem /home/gregor/kde/usr/include/KF5/KConfigCore -isystem /usr/include/qt5/QtNetwork -isystem /usr/include/qt5/QtConcurrent -isystem /usr/include/qt5/QtDBus -isystem /home/gregor/kde/usr/include/KF5/KJobWidgets -isystem /usr/include/qt5/QtWidgets -isystem /usr/include/qt5/QtGui -isystem /home/gregor/kde/usr/include/KF5/KCompletion -isystem /home/gregor/kde/usr/include/KF5/KWidgetsAddons -isystem /home/gregor/kde/usr/include/KF5/KXmlGui -isystem /usr/include/qt5/QtXml -isystem /home/gregor/kde/usr/include/KF5/KConfigWidgets -isystem /home/gregor/kde/usr/include/KF5/KCodecs -isystem /home/gregor/kde/usr/include/KF5/KConfigGui -isystem /home/gregor/kde/usr/include/KF5/KAuth -isystem /home/gregor/kde/usr/include/KF5/KTextWidgets -isystem /home/gregor/kde/usr/include/KF5/SonnetUi -isystem /home/gregor/kde/usr/include/KF5/KI18n -isystem /usr/include/qt5/QtQml -isystem /usr/include/qt5/QtPrintSupport -isystem /home/gregor/kde/usr/include/KF5/KArchive -isystem /home/gregor/kde/usr/include/KF5/KGuiAddons -isystem /home/gregor/kde/usr/include/KF5/KIconThemes -isystem /home/gregor/kde/usr/include/KF5/KItemViews -isystem /home/gregor/kde/usr/include/KF5/SonnetCore -isystem /home/gregor/kde/usr/include/KF5/KSyntaxHighlighting -isystem /home/gregor/kde/usr/include  -pipe -std=c++0x -fno-operator-names -fno-exceptions -Wall -Wextra -Wcast-align -Wchar-subscripts -Wformat-security -Wno-long-long -Wpointer-arith -Wundef -Wnon-virtual-dtor -Woverloaded-virtual -Werror=return-type -Wvla -Wdate-time -pedantic -Wsuggest-override -Wlogical-op -Wzero-as-null-pointer-constant -g -fPIC -fvisibility=hidden -fvisibility-inlines-hidden   -fPIC -std=gnu++11 -o CMakeFiles/KF5TextEditor.dir/buffer/katetexthistory.cpp.o -c /home/gregor/kde/src/frameworks/ktexteditor/src/buffer/katetexthistory.cpp

TODO: see also hints at https://phabricator.kde.org/D16341
