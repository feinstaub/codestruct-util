// from https://github.com/Lekensteyn/qt5printers

#include <QTextStream>
#include <QUrl>
void test(const QByteArray & ba) { }
int main(void) {
    test(QByteArray("abc"));
    QUrl url(QUrl::fromLocalFile("/home/test/hallo.txt"));
    return 0;
}

/**
    g++ main.cpp $(pkg-config --cflags --libs Qt5Core) -g -fPIC
 
    gdb -q -ex break\ test -ex r ./a.out

    # "If everything goes well you should see the expanded data:"
    Breakpoint 1, test (ba="abc" = {...}) at test.cpp:4
    4           test(QByteArray("abc"));
    
    // WITHOUT this .gdbinit
    (gdb) p ba
    $1 = (const QByteArray &) @0x7fffffffdeb8: {
    d = 0x41d100
    }
    
    // WITH this .gdbinit (still looks not quite like the one above)
    (gdb) p ba
    $1 = "abc" = {
        [0] = 97 'a',
        [1] = 98 'b',
        [2] = 99 'c'
    }
*/
