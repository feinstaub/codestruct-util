#include <iostream>
#include <tuple>

// aus cppref ---------------
struct S {
    static const int x = 0; // static data member
    // a definition outside of class is required if it is odr-used
};

const int& f(const int& r);

bool b = true;
int o = S::x; // not odr-used
const int& p = S::x; // odr-used

 int n = b ? (1, S::x) // S::x is not odr-used here
           : f(S::x);  // S::x is odr-used here: a definition is required
// --------------------------

const int S::x;
const int& f(const int& r)
{
    return r;
}

int main()
{
    std::cout << n << std::endl;
    int arr[] = {1, 2, 3}; // OK

    int a = (1, 2, 3); // comma operator; yields last expression result
    std::cout << a << std::endl; // 3

    int b, c, d;
    (a, b, c, d) = (1, 2, 3, 4); // comma operator: a, b and c are unchanged; d gets 4
    printf("%d %d %d %d\n", a, b, c, d);

    return 0;
}

/**
    g++ main.cpp
    ./a.out
*/
