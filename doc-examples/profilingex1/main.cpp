#include <iostream>

// taken from https://dev.to/etcwilde/perf---perfect-profiling-of-cc-on-linux-of


int fib(int x) {
  if (x == 0) return 0;
  else if (x == 1) return 1;
  return fib(x - 1) + fib(x - 2);
}


int main(int argc, char **argv) {
    for (size_t i = 0; i < 45; ++i) {
        std::cout << i << ": " << fib(i) << std::endl;
    }
    return 0;
}
