cmake_minimum_required(VERSION 2.6)
project(profilingex1)

add_executable(profilingex1 main.cpp)

install(TARGETS profilingex1 RUNTIME DESTINATION bin)
