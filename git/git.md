Git
===

One-time Installation
---------------------
Symlink these files to $HOME

    .gitconfig
    .gitignore

Symlink this file to $HOME/.config/git/attributes

    .gitattributes

Install these tools:

    odt2txt

odt2txt
-------
siehe .gitconfig

Splitting a subfolder out into a new repository
-----------------------------------------------
See https://help.github.com/articles/splitting-a-subfolder-out-into-a-new-repository/
See https://git-scm.com/docs/git-filter-branch

Clone the repository to a new location, e.g. newrepo

    cd newrepo

Filter-Branch to extract the folder mysubfolder:

    git filter-branch --prune-empty --subdirectory-filter mysubfolder

Remove an unwanted file
-----------------------
See https://git-scm.com/docs/git-filter-branch

    git filter-branch --index-filter 'git rm --cached --ignore-unmatch unwanted_filename' HEAD

Change author in several commits
--------------------------------
https://stackoverflow.com/questions/4981126/how-to-amend-several-commits-in-git-to-change-author

    git filter-branch --env-filter 'if [ "$GIT_AUTHOR_EMAIL" = "incorrect@email" ]; then
        GIT_AUTHOR_EMAIL=correct@email;
        GIT_AUTHOR_NAME="Correct Name";
        GIT_COMMITTER_EMAIL=$GIT_AUTHOR_EMAIL;
        GIT_COMMITTER_NAME="$GIT_AUTHOR_NAME"; fi' -- --all

TODO: try out!

Change a Remote
---------------

... todo

    git remote rename origin old-origin
    git remote add origin https://framagit.org/feinstaub/shelltux.git
    git push -u origin --all
    git push -u origin --tags

Change author for one or more commits
-------------------------------------
https://stackoverflow.com/questions/3042437/how-to-change-the-commit-author-for-one-specific-commit

    git rebase -i <earliercommit>
    # choose edit
    git commit --amend --author="Author Name <email@address.com>"
    git rebase --continue

Tools
-----
### AutoMergeTool
* https://github.com/xgouchet/AutoMergeTool  todo

### A Better Git Blame
https://blog.andrewray.me/a-better-git-blame/

TODO, in Kate einbauen
